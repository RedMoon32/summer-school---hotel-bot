# Innopolis hotel bot
Проект Летней Проектной Школы Иннополиса 2019.

## Описание
Телеграмм бот помощник для сайта https://hotel.university.innopolis.ru/portal.  
**Бот поддерживает**:
* Создание заявок
* Просмотр информации о закрытых/открытых заявках
* Закрытие/Переоткрытие заявок
* Добавление комментариев к заявкам. 
* Английский и Русский языки
* Напоминание о закрытии заявок
## Запуск через докер
```
$ git clone https://gitlab.com/RedMoon32/summer-school---hotel-bot.git --recursive
$ cd summer-school---hotel-bot
$ docker build --tag=summer-school--hotel-bot .
```
## Использованные технологии
  * python-telegram-bot
  * django
  * requests
  * logger
  * pytest
## Разработчики
  * Абрамов Матвей - [@Karapys](https://gitlab.com/Karapys)
  * Денисов Олег - [@Jktulord](https://gitlab.com/Jktulord)