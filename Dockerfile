# Use python v3.6
FROM python:3.6-slim

# Add requirements to the app before anything else
ADD bot/requirements.txt /app/bot/

# Set the working directory to /app
WORKDIR /app/bot/

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Copy the current directory contents into the container at /app
COPY . /app

# Make port 80 available to the world outside this container
EXPOSE 80

# Apply migrations when the container launches
RUN ["python", "manage.py", "migrate"]

# Run bot
RUN ["python", "manage.py", "run_bot"]
