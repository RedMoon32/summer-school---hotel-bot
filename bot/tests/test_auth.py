import sys
import pytest

sys.path.append("..")

import auth
from secrets import email, password


@pytest.fixture
def logged_user():
    return {"email": email, "password": password}


def test__check_token(logged_user):
    token = auth.get_person_tokens(logged_user["email"], logged_user["password"])
    assert token is not None
