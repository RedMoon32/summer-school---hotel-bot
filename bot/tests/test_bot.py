from unittest.mock import Mock
from core.bot import *
from core.callbacks import callback_opened_req_alert
from core.models import Person
import pytest
from core.enums import *
import sys

sys.path.append("..")
from secrets import email, password
from secrets import test_person as person


@pytest.fixture
def update():
    chat_id = 5555
    update = Mock()
    update.message = Mock()
    update.message.text = Mock()
    update.message.chat_id = chat_id
    update.message.reply_text = Mock()
    update.edit_message_text = Mock()
    update.callback_query = Mock()
    return update


@pytest.fixture
def context():
    context = Mock()
    context.bot = Mock()
    context.chat_data = {}
    return context


def user(update):
    return Person.objects.get(chat_id=update.message.chat_id)


def request(update):
    return Request.objects.get(person=get_user(update))


def handle(update, context):
    p = get_user(update)
    p.input = True
    p.save()
    m.handle(update, context)


@pytest.mark.django_db
def test__full_bot(update, context):
    m.handle(update, context)
    assert user(update).state == States.language
    update.message.text = "RU"

    m.handle(update, context)
    assert user(update).language == Languages.russian
    assert user(update).state == States.email

    update.message.text = email
    m.handle(update, context)
    update.message.text = password
    m.handle(update, context)
    assert user(update).email == email
    assert user(update).password == password
    assert user(update).state == States.menu


@pytest.mark.django_db
def test__send_request(update, context):
    p = get_user(update)
    p.state = States.send_category
    p.save()

    update.message.chat.username = "JackalM"

    update.message.text = "Мебель"
    handle(update, context)
    assert get_request(update).category == update.message.text
    assert user(update).state == States.send_theme

    update.message.text = "Сломался стул"
    handle(update, context)
    assert get_request(update).theme == update.message.text
    assert user(update).state == States.send_description

    update.message.text = "Сломался стул. Почините пожалуйста"
    handle(update, context)
    assert get_request(update).description == update.message.text
    assert user(update).state == States.send_dorm_number

    update.message.text = "1"
    handle(update, context)
    assert get_request(update).dorm_number == update.message.text
    assert user(update).state == States.send_room_number

    update.message.text = "101"
    handle(update, context)
    assert get_request(update).room_number == update.message.text
    assert user(update).state == States.send_date

    update.message.text = "29-08-2019"
    handle(update, context)
    assert get_request(update).date == update.message.text
    assert user(update).state == States.send_time_interval

    update.message.text = "9:00-11:00"
    handle(update, context)
    assert get_request(update).time_interval == update.message.text
    assert user(update).state == States.ask_request_right

    update.message.text = "Да"
    handle(update, context)
    assert user(update).state == States.menu


@pytest.mark.django_db
def test__show_requests(update, context):
    """
    Проверка работы отображения заявок
    """
    p = get_user(update)
    p.email = email
    p.password = password
    login(p, update, context)
    p.state = States.requests
    p.input = True
    p.save()

    update.message.text = "Открытые"
    m.handle(update, context)
    assert get_request(update).person.state == "menu"

    update.message.text = "Мои заявки"
    m.handle(update, context)
    update.message.text = "Закрытые"
    m.handle(update, context)
    assert get_request(update).person.state == "menu"


@pytest.mark.django_db
def test_fail_category_request(update, context):
    p = get_user(update)
    p.state = States.send_category
    p.save()

    update.message.text = "Мебелъ"
    handle(update, context)
    assert user(update).state == States.send_category


@pytest.mark.django_db
def test_send_fail_time_past(update, context):
    p = get_user(update)
    p.state = States.send_date
    p.save()

    update.message.text = "11-07-2019"
    handle(update, context)
    assert user(update).state == States.send_date

    p = get_user(update)
    p.state = States.send_date
    p.save()

    update.message.text = "11-07-2070"
    handle(update, context)
    assert user(update).state == States.send_date


@pytest.mark.django_db
def test_send_fail_time_future(update, context):
    p = get_user(update)
    p.state = States.send_date
    p.save()

    update.message.text = "11-07-2070"
    handle(update, context)
    assert user(update).state == States.send_date


@pytest.mark.django_db
def test_send_fail_dorm_number(update, context):
    p = get_user(update)
    p.state = States.send_dorm_number
    p.save()

    update.message.text = "5"
    handle(update, context)
    assert user(update).state == States.send_dorm_number


@pytest.mark.django_db
def test_send_fail_time_interval(update, context):
    p = get_user(update)
    p.state = States.send_time_interval
    p.save()

    update.message.text = "5"
    handle(update, context)
    assert user(update).state == States.send_time_interval


@pytest.mark.django_db
def test__add_alarm_for_all_users():
    person.save()
    callback_opened_req_alert(update)
