import sys

sys.path.append("..")
from secrets import test_person
from request import HotelParser, URL

REQUEST_URL = "https://hotel.university.innopolis.ru/portal/view-help-request/"


def test__get_site():
    """
        Проверка доступности сайта hotel.university.innopolis.ru
    """
    assert HotelParser._get_site(URL, test_person.get_cookies()) is not None
