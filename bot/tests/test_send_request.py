from request import HotelParser, REQUEST_URL
from core.models import Request
from secrets import test_person as person


def test__send_request():
    req = Request(
        person=person,
        theme="titlePython",
        category="Окна",
        description="desc",
        dorm_number="1",
        room_number="123",
        date="01-08-2019",
        time_interval="9:00-11:00",
        telegram="@hgtyt",
    )
    assert HotelParser.send_request(req) is not None


def test__add_comment():
    HotelParser.add_comment(2666, "my comment", person)
    text, comments = HotelParser.get_request_info(
        REQUEST_URL + "2666", person.get_cookies()
    )
    assert comments[-1]["text"] == "Ticket closed."
