from djchoices import DjangoChoices, ChoiceItem


class States(DjangoChoices):
    language = ChoiceItem()
    email = ChoiceItem()
    password = ChoiceItem()
    check_password_failed = ChoiceItem()
    check_password_complete = ChoiceItem()
    menu = ChoiceItem()
    profile = ChoiceItem()
    send_category = ChoiceItem()
    send_theme = ChoiceItem()
    send_description = ChoiceItem()
    send_dorm_number = ChoiceItem()
    send_room_number = ChoiceItem()
    send_date = ChoiceItem()
    send_time_interval = ChoiceItem()
    show_request = ChoiceItem()
    ask_request_right = ChoiceItem()
    send_all = ChoiceItem()
    begin = ChoiceItem()
    login = ChoiceItem()
    cleaning = ChoiceItem()
    requests = ChoiceItem()
    opened = ChoiceItem()
    closed = ChoiceItem()
    choose_request = ChoiceItem()
    add_comment = ChoiceItem()


class Languages(DjangoChoices):
    russian = ChoiceItem("RU")
    english = ChoiceItem("EN")
