from django.core.management.base import BaseCommand
from core.bot import run_bot
from core.models import Person


class Command(BaseCommand):
    help = "Run telegram bot"

    def handle(self, *args, **options):
        Person.objects.all().delete()
        run_bot()
