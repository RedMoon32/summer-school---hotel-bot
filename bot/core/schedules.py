import urllib.request as req
from constant import schedule_path
from auth import login
from core.models import Person

IMG_URL = "https://hotel.university.innopolis.ru/assets/images/schedule.png"


def relogin_all():
    for person in Person.objects.all():
        login(person, None, None, silent=True)


def download_image():
    req.urlretrieve(IMG_URL, schedule_path)
