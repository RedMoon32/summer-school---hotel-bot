from telegram.ext import Updater, CommandHandler, MessageHandler, CallbackQueryHandler
from telegram.ext.filters import Filters
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup
from auth import login
from constant import *
from core.callbacks import (
    callback_opened_req_alert,
    callback_relogin_all,
    request_callback_handler,
    add_comment_callback_handler,
    toggle_req_callback_handler,
)
from core.enums import States, Languages
from core.messager import MessageTreeProcessor, State
from core.models import Person, Request
from request import HotelParser, REQUEST_URL
from secrets import API_TOKEN
import logging
import datetime

logger = logging.getLogger(__name__)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def get_user(update):
    return Person.objects.get_or_create(chat_id=update.message.chat_id)[0]


def get_request(update):
    return Request.objects.get_or_create(person=get_user(update))[0]


def get_category_keyboard(user, update, context):
    category = REQUEST_CATEGORY_CHOICES[user.lang]
    markup = []
    for i in range(0, len(category), 2):
        markup.append([category[i], category[i + 1]])
    update.message.reply_text(
        text=SEND_CATEGORY_TEXT[user.lang],
        reply_markup=ReplyKeyboardMarkup(
            markup, resize_keyboard=True, one_time_keyboard=True
        ),
    )
    user.input = True


def get_time_interval_keyboard():
    choices = SEND_TIME_INTERVAL_CHOICES
    markup = []
    for i in range(0, 8, 2):
        markup.append([choices[i], choices[i + 1]])
    return markup


def get_cleaning_chedule(user, update, context):
    image = open(schedule_path, "rb")
    update.message.reply_photo(image)
    user.state = States.menu


def get_requests(user: Person, update, context):
    """
    Выводит список заявок выбранного типа ввиде кнопок
    """
    if user.state == States.opened:
        reqs = HotelParser.get_all_requests(user)[0]
        reply_text = OPENED_REQUESTS[user.lang]
        context.chat_data["requests"] = "opened"
    elif user.state == States.closed:
        reqs = HotelParser.get_all_requests(user)[1]
        reply_text = CLOSED_REQUESTS[user.lang]
        context.chat_data["requests"] = "closed"
    else:
        raise Exception("State not found")

    if not reqs:
        update.message.reply_text(NO_REQUESTS_ERROR[user.lang])
    else:
        keyboard = []
        for request in reqs:
            text = f'{request["title"]}  {request["info"]}'
            keyboard.append(
                [
                    InlineKeyboardButton(
                        text,
                        callback_data="show_req$" + request["href"][len(REQUEST_URL) :],
                    )
                ]
            )
            reply_markup = InlineKeyboardMarkup(keyboard)

        update.message.reply_text(reply_text, reply_markup=reply_markup)


def add_comment(user, update, context):
    """
    Добавляет комментарий к заявке и переносит пользователя в меню
    """
    text = update.message.text
    num = context.chat_data["num"]
    HotelParser.add_comment(num, text, user)
    context.bot.send_message(
        chat_id=update.message.chat_id, text=SUCCESSFUL_ADD_COMMENT[user.lang]
    )
    user.state = States.menu
    user.save()


def show_request(user, update, context):
    req = get_request(update)
    message_text = SHOW_REQUEST_TEXT[user.lang].format(
        req.category,
        req.theme,
        req.description,
        req.dorm_number,
        req.room_number,
        req.date,
        req.time_interval,
    )
    update.message.reply_text(message_text)


def send_request(user, update, context):
    """
    Отправка запроса на сервер на публикацию заявки.
    Добавление в заявку поле телеграм
    """
    request = get_request(update)
    # Adding telegram username
    request.telegram = "@" + update.message.chat.username
    request.save()
    num = HotelParser.send_request(request)
    if num:
        update.message.reply_text(SUCCESSFUL_SEND_REQUEST[user.lang] + num)
    else:
        update.message.reply_text(ERROR_WHILE_SEND_REQUEST[user.lang])


message_tree = {
    States.begin: State(States.language, WELCOMING_MESSAGE),
    States.language: State(
        States.email,
        BUTTON_UPPER_TEXT_LANGUAGE,
        menu_buttons=[[Languages.russian, Languages.english]],
        change_field="language",
        change_instance=get_user,
        any_language=True,
    ),
    States.email: State(
        States.password, ENTER_EMAIL, change_field="email", change_instance=get_user
    ),
    States.password: State(
        States.login, ENTER_PASSWORD, change_field="password", change_instance=get_user
    ),
    States.check_password_complete: State(States.menu, CHECK_PASSWORD_COMPLETE),
    States.check_password_failed: State(States.email, CHECK_PASSWORD_FAILED),
    States.login: State(None, custom_func=login),
    States.menu: State(
        None,
        REQUEST_CATEGORY_REPLY_TEXT,
        CHOICES_MAIN_MENU,
        message_states={
            0: States.menu,
            1: States.send_category,
            2: States.requests,
            3: States.cleaning,
        },
    ),
    States.profile: State(None, "Profile"),
    States.send_category: State(
        States.send_theme,
        custom_func=get_category_keyboard,
        change_instance=get_request,
        change_field="category",
    ),
    States.send_theme: State(
        States.send_description,
        SEND_THEME_TEXT,
        change_instance=get_request,
        change_field="theme",
    ),
    States.send_description: State(
        States.send_dorm_number,
        SEND_DESCRIPTION_TEXT,
        change_instance=get_request,
        change_field="description",
    ),
    States.send_dorm_number: State(
        States.send_room_number,
        SEND_DORM_NUMBER_TEXT,
        menu_buttons=[DORM_NUMBER_CHOICES],
        change_instance=get_request,
        change_field="dorm_number",
        any_language=True,
    ),
    States.send_room_number: State(
        States.send_date,
        SEND_ROOM_NUMBER_TEXT,
        change_instance=get_request,
        change_field="room_number",
    ),
    States.send_date: State(
        States.send_time_interval,
        SEND_DATE_TEXT,
        change_instance=get_request,
        change_field="date",
    ),
    States.send_time_interval: State(
        States.show_request,
        SEND_TIME_INTERVAL_TEXT,
        menu_buttons=get_time_interval_keyboard(),
        change_instance=get_request,
        change_field="time_interval",
        any_language=True,
    ),
    States.show_request: State(States.ask_request_right, custom_func=show_request),
    States.ask_request_right: State(
        None,
        SEND_END_TEXT,
        menu_buttons=SEND_END_CHOICES,
        message_states={0: States.send_all, 1: States.menu},
    ),
    States.send_all: State(States.menu, custom_func=send_request),
    States.cleaning: State(States.menu, custom_func=get_cleaning_chedule),
    States.requests: State(
        None,
        REQUEST_TYPE_OF_REQUESTS,
        CHOICES_REQUESTS,
        message_states={0: States.opened, 1: States.closed},
    ),
    States.opened: State(States.menu, custom_func=get_requests),
    States.closed: State(States.menu, custom_func=get_requests),
    States.add_comment: State(None, custom_func=add_comment),
}

m = MessageTreeProcessor(message_tree)


def run_bot():
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )
    updater = Updater(API_TOKEN, use_context=True)
    job = updater.job_queue
    updater.dispatcher.add_handler(CommandHandler("start", m.handle))
    updater.dispatcher.add_handler(
        MessageHandler(filters=Filters.all, callback=m.handle)
    )

    # Inline button callbacks
    updater.dispatcher.add_handler(
        CallbackQueryHandler(request_callback_handler, pattern="show_req")
    )
    updater.dispatcher.add_handler(
        CallbackQueryHandler(add_comment_callback_handler, pattern="add_comment")
    )
    updater.dispatcher.add_handler(
        CallbackQueryHandler(
            toggle_req_callback_handler, pattern=r"close_req|open__req"
        )
    )

    # Job

    timezone_delta = -3
    job.run_daily(
        callback_opened_req_alert,
        time=datetime.time(hour=12 + timezone_delta, minute=0, second=0),
    )
    job.run_daily(
        callback_relogin_all,
        time=datetime.time(hour=4 + timezone_delta, minute=0, second=0),
    )

    updater.dispatcher.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    run_bot()
