from django.core.exceptions import ValidationError
from django.db import models
from core.enums import States, Languages
from datetime import datetime
from constant import (
    DORM_NUMBER_CHOICES,
    SEND_TIME_INTERVAL_CHOICES,
    REQUEST_CATEGORY_CHOICES,
    MIN_DAYS,
    MAX_DAYS,
)


class Person(models.Model):
    chat_id = models.IntegerField()
    language = models.CharField(
        max_length=7, choices=Languages.choices, default=Languages.russian
    )
    email = models.EmailField(max_length=254, null=True)
    password = models.CharField(max_length=30, null=True)
    state = models.CharField(
        max_length=20, choices=States.choices, null=True, default=States.begin
    )

    auth_token = models.CharField(max_length=254, null=True)
    end_user_token = models.CharField(max_length=254, null=True)
    spiceworks_session = models.CharField(max_length=512, null=True)
    pickaxe = models.CharField(max_length=10, null=True)

    input = models.BooleanField(default=False)

    def validate(self):
        if self.language != Languages.russian and self.language != Languages.english:
            raise ValidationError(message="invalid language")

    def get_cookies(self):
        cookies = {
            "remember_end_user_token": self.end_user_token,
            "spiceworks_session": self.spiceworks_session,
        }
        return cookies

    @property
    def lang(self):
        if self.language == Languages.russian:
            language = 0
        else:
            language = 1
        return language

    def __repr__(self):
        return f"Person with chat_id {self.chat_id}"


class Request(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    auth_token = models.CharField(max_length=254, null=True)
    category = models.CharField(max_length=7, null=True)
    theme = models.CharField(max_length=100, null=True)
    description = models.CharField(max_length=1000, null=True)
    dorm_number = models.CharField(max_length=1, default=DORM_NUMBER_CHOICES[0])
    room_number = models.CharField(max_length=300, null=True)
    date = models.CharField(max_length=20, null=True)
    time_interval = models.CharField(
        max_length=20, null=True, default=SEND_TIME_INTERVAL_CHOICES[0]
    )
    telegram = models.CharField(max_length=50, null=True)

    def validate(self):
        if (
            not self.dorm_number in DORM_NUMBER_CHOICES
            or not self.time_interval in SEND_TIME_INTERVAL_CHOICES
            or not self.category in REQUEST_CATEGORY_CHOICES[self.person.lang]
        ):
            raise ValidationError(message="Error")
        if not self.date is None:
            try:
                if self.date:
                    req_date = datetime.strptime(self.date, "%d-%m-%Y").date()
                    now_date = datetime.now().date()
                    if not MIN_DAYS < (req_date - now_date).days < MAX_DAYS:
                        raise ValueError("Error")
            except ValueError:
                raise ValidationError(message="Error")
