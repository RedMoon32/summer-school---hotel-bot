from typing import List, Callable
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove

from constant import ERROR
from core.models import Person


class State:
    def __init__(
        self,
        new_state: str,
        send_text: str = None,
        menu_buttons: List = None,
        change_field: str = None,
        change_instance: Callable = None,
        custom_func: Callable = None,
        message_states=None,
        any_language=False,
    ):
        self.send_text = send_text
        self.new_state = new_state
        self.menu_buttons = menu_buttons
        self.change_field = change_field
        self.change_instance = change_instance
        self.custom_func = custom_func
        self.message_states = message_states
        self.any_language = any_language

    def process(self, user, update, context):
        language = user.lang
        if self.any_language:
            target = self.menu_buttons
        elif self.menu_buttons:
            target = self.menu_buttons[language]

        if self.message_states and user.input:
            if update.message.text in target:
                index = target.index(update.message.text)
                user.state = self.message_states[index]
                user.input = False
                user.save()
                return True
            else:
                context.bot.send_message(
                    chat_id=update.message.chat_id, text=ERROR[user.lang]
                )
                return False
        if self.custom_func and not user.input:  # если обрабатываем ручками
            self.custom_func(user, update, context)
        elif (
            not user.input
        ):  # если от пользователя не ожидалось никакого сообщения отправляем ему
            # текст или кнопки
            if self.send_text:
                if self.menu_buttons:  # если есть меню отправляем меню
                    if self.any_language:
                        target = self.menu_buttons
                    else:
                        target = [self.menu_buttons[language]]
                    keyboard = ReplyKeyboardMarkup(
                        target, resize_keyboard=True, one_time_keyboard=True
                    )
                else:
                    keyboard = ReplyKeyboardRemove()
                context.bot.send_message(
                    chat_id=update.message.chat_id,
                    text=self.send_text[language],
                    reply_markup=keyboard,
                )
            if (
                self.change_field or self.message_states
            ):  # если в этом состоянии требуется
                # что-то ввести то после отправки текста ждем сообщение
                user.input = True
        else:
            try:
                instance = self.change_instance(update)
                setattr(
                    instance, self.change_field, update.message.text
                )  # пользователь что-то прислал, записываем в базу данных
                instance.validate()
                instance.save()
                user = Person.objects.get(chat_id=update.message.chat_id)
                user.input = False

            except:
                context.bot.send_message(
                    chat_id=update.message.chat_id, text=ERROR[user.lang]
                )  # не удалось
        if not user.input and self.new_state is not None:
            user.state = self.new_state
        user.save()
        return not user.input


class MessageTreeProcessor:
    def __init__(self, message_tree):
        self.message_tree = message_tree
        self.first = True

    def handle(self, update, context):
        p = Person.objects.get_or_create(chat_id=update.message.chat_id)[0]

        state = self.message_tree[p.state]
        res = state.process(p, update, context)

        if res is True:
            self.handle(update, context)
