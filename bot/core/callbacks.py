from unittest.mock import Mock

from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from constant import (
    OPENED_REQUESTS_ALERT,
    COMMENTS,
    NOT_COMMENTS,
    ADD_COMMENT,
    CLOSE_REQUEST,
    OPEN_REQUEST,
    TEXT_OF_COMMENT,
    SUCCESSFUL_CLOSE_REQUEST,
    ERROR_WHILE_CLOSE_REQUEST,
    SUCCESSFUL_OPEN_REQUEST,
    ERROR_WHILE_OPEN_REQUEST,
    LINE,
)
from request import REQUEST_URL
from core.enums import States
from core.models import Person
from core.schedules import relogin_all
from request import HotelParser


def callback_opened_req_alert(context):
    """
    Если у пользователя есть открытые заявки старше 4 дней,
    отправляет ему сообщение с просьбой закрыть заявку
    и вызывает request_callback_handler
    """
    all_users = Person.objects.all()
    for user in all_users:
        opened_reqs, closed_reqs = HotelParser.get_all_requests(user)
        for req in opened_reqs:
            delta, num = HotelParser.get_delta_and_num(req)
            if delta % 4 == 0 and delta != 0:
                update = Mock()
                update.callback_query = Mock()
                update.callback_query.data = "show_req$" + str(num)
                update.callback_query.message.chat_id = user.chat_id
                context.bot.send_message(
                    chat_id=user.chat_id, text=OPENED_REQUESTS_ALERT[user.lang]
                )
                context.chat_data = {"requests": "opened"}
                request_callback_handler(update, context)
                break


def callback_relogin_all(context):
    relogin_all()


def request_callback_handler(update, context):
    """
    Обработка нажатия пользователя на инлайн кнопку заявки и отправка ему информации о ней
    Вывод кнопок закрыть/открыть заявку и оставить комментарий
    """
    user = Person.objects.get(chat_id=update.callback_query.message.chat_id)
    cookies = user.get_cookies()
    num = str(update.callback_query.data)[len("show_req$") :]
    text, comments = HotelParser.get_request_info(REQUEST_URL + num, cookies)
    context.bot.send_message(chat_id=update.callback_query.message.chat_id, text=text)
    all_comments = LINE[user.lang] + f"{COMMENTS[user.lang]}: \n"
    for comment in comments:
        all_comments += comment["text"] + "\n"
        all_comments += comment["meta"] + "\n\n"
    if not comments:
        all_comments = LINE[user.lang] + NOT_COMMENTS[user.lang]

    # Add comment and close request button
    keyboard = [
        [
            InlineKeyboardButton(
                ADD_COMMENT[user.lang], callback_data="add_comment$" + num
            )
        ]
    ]
    if context.chat_data["requests"] == "opened":
        keyboard.append(
            [
                InlineKeyboardButton(
                    CLOSE_REQUEST[user.lang], callback_data="close_req$" + num
                )
            ]
        )
    else:
        keyboard.append(
            [
                InlineKeyboardButton(
                    OPEN_REQUEST[user.lang], callback_data="open__req$" + num
                )
            ]
        )
    reply_markup = InlineKeyboardMarkup(keyboard)

    context.bot.send_message(
        chat_id=update.callback_query.message.chat_id,
        text=all_comments,
        reply_markup=reply_markup,
    )


def add_comment_callback_handler(update, context):
    """
    Обрабатывает нажатие пользователя на кнопку 'добавить комментарий'
    """
    num = update.callback_query.data[len("add_comment$") :]
    context.chat_data["num"] = num
    user = Person.objects.get(chat_id=update.callback_query.message.chat_id)
    context.bot.send_message(
        chat_id=update.callback_query.message.chat_id, text=TEXT_OF_COMMENT[user.lang]
    )
    user.state = States.add_comment
    user.input = False
    user.save()


def toggle_req_callback_handler(update, context):
    """
    Обрабатывает нажиатия пользователя на кнопку 'закрыть/открыть заявку'
    Закрывает/открывает заявку на сервере и переносит пользователя в меню
    """
    user = Person.objects.get(chat_id=update.callback_query.message.chat_id)
    if update.callback_query.data.split("$")[0] == "close_req":
        success_text = SUCCESSFUL_CLOSE_REQUEST[user.lang]
        error_text = ERROR_WHILE_CLOSE_REQUEST[user.lang]
    elif update.callback_query.data.split("$")[0] == "open__req":
        success_text = SUCCESSFUL_OPEN_REQUEST[user.lang]
        error_text = ERROR_WHILE_OPEN_REQUEST[user.lang]
    else:
        raise Exception("Wrong Callback")

    num = update.callback_query.data[len("close_req$") :]
    user = Person.objects.get(chat_id=update.callback_query.message.chat_id)
    status_code = HotelParser.toggle_request(num, user)
    if status_code == 302:
        context.bot.send_message(
            chat_id=update.callback_query.message.chat_id, text=success_text
        )
    else:
        context.bot.send_message(
            chat_id=update.callback_query.message.chat_id, text=error_text
        )
    user.state = States.menu
    user.save()
