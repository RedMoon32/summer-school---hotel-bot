from core.models import Person
import requests_html
from core.models import Request
import requests
from typing import Dict, List, Tuple
from requests_toolbelt import MultipartEncoder
import re
import datetime

SCHEME = "https://hotel.university.innopolis.ru"
SUBMIT_URL = (
    "https://hotel.university.innopolis.ru/user_portal/custom_ticket_forms/1/submit"
)
REQUEST_URL = "https://hotel.university.innopolis.ru/portal/view-help-request/"
URL = "https://hotel.university.innopolis.ru/portal"


class HotelParser:
    @staticmethod
    def _parse_requests(site, css_selector: str):
        """
        Парсинг заявок со страницы, используя css selector
        """
        requests_elements = site.html.find(css_selector)[0].find("li")
        parsed_requests = []
        if requests_elements:
            for _request in requests_elements:
                title = _request.find("a")[0].text
                if len(_request.links) == 1:
                    href = SCHEME + _request.links.pop()
                else:
                    href = ""
                info = _request.find("span")[0].text
                parsed_requests.append({"title": title, "href": href, "info": info})
        return parsed_requests

    @staticmethod
    def _get_site(
        url: str, cookies: Dict = None, allow_redirects=False
    ) -> requests_html.HTMLResponse or None:
        """
        Получение экземпляра сайта и отлов возможных ошибок
        """
        session = requests_html.HTMLSession()
        try:
            if cookies:
                site = session.get(
                    url, cookies=cookies, allow_redirects=allow_redirects
                )
            else:
                site = session.get(url, allow_redirects=allow_redirects)
            if site.status_code not in (200, 302):
                raise Exception("Status code is not 200 or 302")
        except Exception:
            return None
        return site

    @staticmethod
    def get_all_requests(
        user: Person
    ) -> Tuple[List[Dict[str, str]], List[Dict[str, str]]]:
        """
        Получение всех созданных заявок пользователя с сайта hotel.university.innopolis.ru
        :returns: Открытые и закрытые заявки пользователя с информацией о
        title, href, str с номером тикета и времени в meta
        """
        cookies = user.get_cookies()
        url = SCHEME + "/portal/page/56-"
        site = HotelParser._get_site(url, cookies)

        opened_requests = HotelParser._parse_requests(site, OPENED_REQUESTS_SELECTOR)
        closed_requests = HotelParser._parse_requests(site, CLOSED_REQUESTS_SELECTOR)

        return opened_requests, closed_requests

    @staticmethod
    def get_request_info(url: str, cookies: Dict) -> Tuple[str, List[Dict[str, str]]]:
        """
        Получение инфомации о конкретной заявке пользователя с сайта hotel.university.innopolis.ru
        """
        site = HotelParser._get_site(url, cookies)

        text = site.html.find(REQUEST_TEXT_SELECTOR, first=True).text
        el_comments = site.html.find(REQUEST_COMMENTS_SELECTOR, first=True)
        comments = []
        for comment in el_comments.find("li"):
            body = comment.find("p.body", first=True)
            if not body:
                continue
            else:
                body = body.text
            comments.append(
                {"text": body, "meta": comment.find("p.meta", first=True).text}
            )
        return text, comments

    @staticmethod
    def send_request(request: Request) -> str or None:
        """
        Отправляет запрос на публикацию новой заявки на сайт
        :return: Номер заявки из url
        """
        cookies = request.person.get_cookies()
        form_id = "custom-ticket-form-1_1503321076"

        data = {
            "authenticity_token": request.person.auth_token,
            "_pickaxe": request.person.pickaxe,
            "element_id": form_id,
            "custom_ticket_form_field_37": request.category,
            "custom_ticket_form_field_33": request.theme,
            "custom_ticket_form_field_3": request.description,
            "custom_ticket_form_field_5": request.dorm_number,
            "custom_ticket_form_field_2": request.room_number,
            "custom_ticket_form_field_43[date]": request.date,
            "custom_ticket_form_field_44": request.time_interval,
            "custom_ticket_form_field_7": request.telegram,
            "dom_context": form_id,
        }

        encoder = MultipartEncoder(data)

        response = requests.post(
            SUBMIT_URL,
            headers={"Content-Type": encoder.content_type},
            data=encoder,
            cookies=cookies,
        )
        pattern = r'portal/view-help-request/[^"]+'
        try:
            index = re.search(pattern, response.history[0].text).group(0)
            index = index[len("portal/view-help-request/") :]
            return index
        except Exception:
            return None

    @staticmethod
    def toggle_request(num: int, user):
        """
        Переключает состояние заявки.
        Открытая -> Закрытая и наоборот
        """
        url = SCHEME + f"/portal/toggle_ticket/{num}"

        site = HotelParser._get_site(url, user.get_cookies())
        return site.status_code

    @staticmethod
    def add_comment(num: int, comment: str, user: Person):
        """
        Добавляет комментарий к открытой заявке
        """
        url = SCHEME + f"/portal/add_comment/{num}"

        data = {
            "authenticity_token": user.auth_token,
            "comment[body]": comment,
            "comment[attachment]": b"",
        }

        encoder = MultipartEncoder(data)

        requests.post(
            url,
            headers={"Content-Type": encoder.content_type},
            data=encoder,
            cookies=user.get_cookies(),
        )

    @staticmethod
    def get_date_and_num_from_request(request: Dict[str, str]):
        hour, minute, sec = re.search(r"..:..:..", request["info"]).group(0).split(":")
        day, month, year = re.search(r"..-..-....", request["info"]).group(0).split("-")
        num = int(re.search(r"#....", request["info"]).group(0)[1:])
        return (
            num,
            datetime.datetime(
                year=int(year),
                month=int(month),
                day=int(day),
                hour=int(hour),
                minute=int(minute),
                second=int(sec),
            ),
        )

    @staticmethod
    def get_delta_and_num(req: Dict[str, str]):
        num, date = HotelParser.get_date_and_num_from_request(req)
        date = date.date()
        now = datetime.datetime.now()
        now = now.replace(hour=now.hour + 3).date()
        delta = (now - date).days
        return delta, num


REQUEST_TEXT_SELECTOR = "#block3_b7 > div > p.body"
REQUEST_COMMENTS_SELECTOR = "#block3_b7 > div > ol"
OPENED_REQUESTS_SELECTOR = "#block2_b5 > ul"
CLOSED_REQUESTS_SELECTOR = "#block4_b6 > ul"
