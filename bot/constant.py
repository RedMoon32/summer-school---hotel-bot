import os
import datetime

schedule_path = "schedule.jpg"

"""Текст"""
ERROR = ["Произошла ошибка", "Error. Try again"]
BUTTON_UPPER_TEXT_LANGUAGE = [
    "Выберите язык\Chose language",
    "Выберите язык\Chose language",
]
AUTH_NOTIFICATION = [
    "Чтобы авторизоватся воспользуйтесь командой /login",
    "To Login use command /login",
]
ENTER_EMAIL = ["Введите Email", "Enter email"]
ENTER_PASSWORD = ["Введите пароль", "Enter password"]
CHECK_PASSWORD_COMPLETE = ["Авторизация завешена", "Authorization complete"]
CHECK_PASSWORD_FAILED = ["Пароль или логин неверен", "Password or Login is incorrect"]
WELCOMING_MESSAGE = [
    "Привет! Я ваш автоматизированный помощник по подаче заявок на портале кампуса "
    "университета. Вот что я умею: принемать заявки проживающих по техническим неполадкам "
    "и другим вопроса касающихся проживания в кампусе. Для начала выберите кнопку 'подать заявку'↓",
    "Hello! I'm your automatized request helper. That's what I can: recieve requests of dormitary habitats "
    "about tech issues and other questions. For the begining choose button 'send request'↓",
]
REQUEST_CATEGORY_REPLY_TEXT = ["Выберите категорию", "Choose category"]
REQUEST_TEXT_REPLY_TEXT = ["Опишите свою проблему", "Describe your problem"]
SEND_CATEGORY_TEXT = [
    "Подача заявки, отправьте категорию",
    "sending request, choose category",
]
SEND_THEME_TEXT = ["Напишите тему заявки", "Write the theme of request"]
SEND_DESCRIPTION_TEXT = ["Опишите свою проблему", "Describe your problem"]
SEND_DORM_NUMBER_TEXT = ["Номер общежития", "Number of dorm"]
SEND_ROOM_NUMBER_TEXT = ["Номер комнаты", "Number of room"]
example_date = datetime.datetime.now() + datetime.timedelta(1)
day = example_date.day
month = example_date.month
if month not in (10, 11, 12):
    month = "0" + str(month)
year = example_date.year
SEND_DATE_TEXT = [
    f"Дата, когда вы будете свободны (Пример: {day}-{month}-{year})",
    f"Date when you will be free(example: {day}-{month}-{year})",
]
SEND_TIME_INTERVAL_TEXT = [
    "Временной интервал, когда к вам можно зайти",
    "Time interval when you will be ready",
]
SEND_END_TEXT = ["Отправить запрос?", "Send request?"]
SEND_ALL_TEXT = ["Отправленно", "Request sent"]
SUCCESSFUL_SEND_REQUEST = [
    "Успешно отправлено. Номер вашей заявки: ",
    "Request has been send to portal!",
]
ERROR_WHILE_SEND_REQUEST = ["Ошибка. Заявка не отправлена", "Error, request not send"]
NO_REQUESTS_ERROR = ["Заявок не найдено", "No requests found"]
REQUEST_TYPE_OF_REQUESTS = [
    "Выберите, какие заявки отобразить",
    "Choose requests to show",
]
OPENED_REQUESTS = ["Открытые заявки", "Opened Requests"]
CLOSED_REQUESTS = ["Закрытые заявки", "Closed Requests"]
ADD_COMMENT = ["Добавить комментарий", "Add comment"]
CLOSE_REQUEST = ["Закрыть заявку", "Close request"]
OPEN_REQUEST = ["Открыть заявку", "Reopen request"]
TEXT_OF_COMMENT = ["Введите текст комментария", "Enter your comment"]
SUCCESSFUL_CLOSE_REQUEST = ["Заявка успешно закрыта", "Request successfully closed"]
ERROR_WHILE_CLOSE_REQUEST = ["Не удалось закрыть заявку", "Request close failed"]
SUCCESSFUL_OPEN_REQUEST = ["Заявка успешно открыта", "Request successfully opened"]
ERROR_WHILE_OPEN_REQUEST = ["Не удалось открыть заявку", "Request re-open failed"]
SUCCESSFUL_ADD_COMMENT = ["Коммент успешно добавлен", "Comment successfully added"]
LINE = ["---------------\n", "---------------\n"]
COMMENTS = ["**Комментарии**", "**Comments**"]
NOT_COMMENTS = ["**Комментариев нет**", "**No comments found**"]
OPENED_REQUESTS_ALERT = [
    "У вас есть открытые заявки, закройте их пожалуйста",
    "You have opened requests, please close them",
]

SHOW_REQUEST_TEXT = [
    "Ваша заявка: \n"
    "Категория: {} \n"
    "Тема: {} \n"
    "Описание: {} \n"
    "Номер кампуса: {} \n"
    "Номер комнаты: {} \n"
    "Свободное время: {} {}",
    "Your request: \n"
    "Category: {} \n"
    "Theme: {} \n"
    "Description: {} \n"
    "Dorm number: {} \n"
    "Room number: {} \n"
    "Available time: {} {}",
]

CHOICES_MAIN_MENU = [
    ["Меню", "Подать заявку", "Мои заявки", "График уборки"],
    ["Menu", "Create request", "My requests", "Cleaning schedule"],
]
CHOICES_REQUESTS = [["Открытые", "Закрытые"], ["Opened", "Closed"]]

REQUEST_CATEGORY_CHOICES_RU = [
    "Сантехника",
    "Электрика",
    "Замки",
    "Окна",
    "Мебель",
    "Интернет",
    "Внос и вынос материальных ценностей",
    "Клининг",
    "Отказ от уборки",
    "Прочее",
]

REQUEST_CATEGORY_CHOICES_EN = [
    "Plumbing",
    "Electrician",
    "Locks",
    "Window",
    "Furniture",
    "The Internet",
    "Entry and removal of material values",
    "Cleaning",
    "Refusal of cleaning",
    "Other",
]

REQUEST_CATEGORY_CHOICES = [REQUEST_CATEGORY_CHOICES_RU, REQUEST_CATEGORY_CHOICES_EN]

DORM_NUMBER_CHOICES = ["1", "2", "3", "4"]
SEND_END_CHOICES = [["Да", "Нет"], ["Yes", "No"]]
SEND_TIME_INTERVAL_CHOICES = [
    "9:00-11:00",
    "10:00-12:00",
    "11:00-13:00",
    "12:00-14:00",
    "13:00-15:00",
    "14:00-16:00",
    "15:00-17:00",
    "16:00-18:00",
]

MIN_DAYS = 0
MAX_DAYS = 30
