import requests_html
import requests
import re
from request import URL
from core.enums import States

AUTH_CSS_SELECTOR = "#block2_b3 > form > div:nth-child(1) > input[type=hidden]"
PICK_CSS_SELECTOR = "#block2_b3 > form > div:nth-child(2) > input[type=hidden]"


def login(person, update, context, silent=False):
    """
    Логин на сайт. При успешном получении токена добавляет его в базу данных
    вместе с pickaxe, spiceworks_session, auth_token и end_user_token
    """
    password = person.password
    email = person.email
    end_user_token, pickaxe, spiceworks_session, auth_token = get_person_tokens(
        email, password
    )
    if end_user_token:
        person.end_user_token = end_user_token
        person.pickaxe = pickaxe
        person.spiceworks_session = spiceworks_session
        person.auth_token = auth_token
        person.save()
        # OK
        if not silent:
            person.state = States.check_password_complete
        return end_user_token
    else:
        # Fail
        if not silent:
            person.state = States.check_password_failed
        return False


def get_person_tokens(email: str, password: str):
    """
    Поулчаем все данные для валидации пользователя на сайте:
    end_user_token, pickaxe, spiceworks_session, auth_token
    """
    url_login = URL + "/session"
    url_parse = URL + "/portal-login"

    # Connect to site to parse data
    session = requests_html.HTMLSession()
    try:
        site = session.get(url_parse)
        if site.status_code is not 200:
            raise
    except Exception:
        return None
    # Parsing auth_token, pickaxe and cookies
    auth_token = site.html.find(AUTH_CSS_SELECTOR, first=True).attrs["value"]
    pickaxe = site.html.find(PICK_CSS_SELECTOR, first=True).attrs["value"]
    cookies = {"spiceworks_session": site.cookies["spiceworks_session"]}
    spiceworks_session = site.cookies["spiceworks_session"]

    data = {
        "authenticity_token": auth_token,
        "_pickaxe": pickaxe,
        "end_user[email]": email,
        "end_user[password]": password,
    }

    # Trying to get set-cookie
    r = requests.post(url_login, data=data, cookies=cookies, allow_redirects=False)
    set_cookie = r.headers["Set-cookie"]
    try:
        end_user_token = re.search(r"remember_end_user_token=[^;]+", set_cookie).group(
            0
        )
        end_user_token = end_user_token[len("remember_end_user_token=") :]
    except Exception:
        end_user_token = None

    return end_user_token, pickaxe, spiceworks_session, auth_token
